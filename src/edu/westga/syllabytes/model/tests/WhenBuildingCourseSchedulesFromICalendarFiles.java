package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

/**
 * Test class to test the functionality of the loadFromFile method in the
 * CourseSchedule class
 * 
 * @author Tammy Lee
 * 
 * @version CS3211
 *
 */
public class WhenBuildingCourseSchedulesFromICalendarFiles {

	/**
	 * Creates a temporary folder for test files
	 */

	private CourseSchedule schedule;

	/**
	 * Set up everything necessary to run tests
	 * 
	 * @throws Exception
	 *             This should not happen
	 */
	@Before
	public void setUp() throws Exception {
		this.schedule = new CourseSchedule();

	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testFirstLecturePeriodOfCourseScheduleCalendarOne() {
		File testFile = new File("calendar.ics");
		this.schedule.loadFromFile(testFile);
		assertEquals("Tattoo Appointment", this.schedule.getSchedule().get(1).titlePropety().getValue());
		assertEquals("Eating yummy things\n", this.schedule.getSchedule().get(1).descriptionProperty().getValue());
	}

	/**
	 * Tests for the last lecture period added to the CourseSchedule
	 */
	@Test
	public void testLastLecturePeriodOfCourseScheduleCalendarOne() {
		File testFile = new File("calendar.ics");
		this.schedule.loadFromFile(testFile);
		int last = this.schedule.getSchedule().size() - 1;
		assertEquals("Aerial Silks", this.schedule.getSchedule().get(last).titlePropety().getValue());
		assertEquals("Circus things\n", this.schedule.getSchedule().get(last).descriptionProperty().getValue());
	}

	/**
	 * Tests for the middle lecture period added to the CourseSchedule
	 */
	@Test
	public void testMiddleLecturePeriodOfCourseScheduleCalendarOne() {
		File testFile = new File("calendar.ics");
		this.schedule.loadFromFile(testFile);
		int middle = this.schedule.getSchedule().size() / 2;
		assertEquals("Brunch", this.schedule.getSchedule().get(middle).titlePropety().getValue());
		assertEquals("Eating yummy things\n", this.schedule.getSchedule().get(middle).descriptionProperty().getValue());
	}

	/**
	 * Tests for the first recurring lecture period added to the CourseSchedule
	 */
	@Test
	public void testDailyRecurringEventCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first single lecture period added to the CourseSchedule
	 */
	@Test
	public void testSingleEventCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testWeeklyRecurringEventCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testMonthlyRecurringEventCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests multiple single events on one day
	 */
	@Test
	public void testMultipleSingleEventsOnOneDayCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests multiple recurring events on one day
	 */
	@Test
	public void testMultipleRecurringEventsOnOneDayCourseScheduleCalendarOne() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testFirstLecturePeriodOfCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the last lecture period added to the CourseSchedule
	 */
	@Test
	public void testLastLecturePeriodOfCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the middle lecture period added to the CourseSchedule
	 */
	@Test
	public void testMiddleLecturePeriodOfCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first recurring lecture period added to the CourseSchedule
	 */
	@Test
	public void testDailyRecurringEventCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first single lecture period added to the CourseSchedule
	 */
	@Test
	public void testSingleEventCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testWeeklyRecurringEventCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests for the first lecture period added to the CourseSchedule
	 */
	@Test
	public void testMonthlyRecurringEventCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests multiple single events on one day
	 */
	@Test
	public void testMultipleSingleEventsOnOneDayCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}

	/**
	 * Tests multiple recurring events on one day
	 */
	@Test
	public void testMultipleRecurringEventsOnOneDayCourseScheduleCalendarTwo() {
		fail("Not yet implemented");
	}
}
