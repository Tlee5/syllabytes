package edu.westga.syllabytes.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import javafx.collections.FXCollections;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateList;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.RRule;

/**
 * Class that models a course schedule by keeping track of lecture period
 * objects
 * 
 * @author Tammy Lee
 * 
 * @version CS3211
 *
 */
public class CourseSchedule extends ModifiableObservableListBase<LecturePeriod> implements Comparator<LecturePeriod> {
	private ObservableList<LecturePeriod> list;

	/**
	 * Creates a new CourseSchedule and initializes the observable list
	 */
	public CourseSchedule() {
		this.list = FXCollections.observableArrayList();
	}

	/**
	 * Populates the course schedule with lecture periods from the icalFile
	 * 
	 * @precondition icalFile != null
	 * 
	 * @postcondition Course schedule is populated with lecture period objects
	 *                from icalFile
	 * @param icalFile
	 *            Ical file for the course schedule to be created from
	 */
	public void loadFromFile(File icalFile) {
		try {
			FileInputStream input = new FileInputStream(icalFile);
			CalendarBuilder builder = new CalendarBuilder();
			Calendar calendar = builder.build(input);
			for (Iterator count = calendar.getComponents(Component.VEVENT).iterator(); count.hasNext();) {
				VEvent component = (VEvent) count.next();
				if (component instanceof VEvent) {
					LecturePeriod lecturePeriod = new LecturePeriod((VEvent) component);
					if (component.getProperty(Property.RRULE) != null) {
						DateList dates = getRecurringDatesForRrule((VEvent) component);
						for (Object currentDate : dates) {
							Date objectDate = (Date)currentDate;
							LocalDate converter = objectDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							lecturePeriod.dateProperty().set(converter);
							this.list.add(lecturePeriod);
							System.out.println(converter);
							
						}
					} else {
						this.list.add(lecturePeriod);
					}
				}
			}
			this.list.sort(this);

		} catch (FileNotFoundException fnfe) {
			System.out.println("File does not exist!");
		} catch (ParserException pe) {
			System.out.println("ParserError");
		} catch (IOException ioe) {
			System.out.println("IOExceptionError");
		} catch (ParseException pe) {
			System.out.println("Error loading file.");
		}
	}

	/**
	 * Gets recurring dates for RRule
	 * 
	 * @param component
	 *            Component to extract RRule from
	 * @return the DateList for the RRule
	 * @throws ParseException
	 */
	private DateList getRecurringDatesForRrule(VEvent component) throws ParseException {
		RRule eventRule = new RRule(component.getProperty(Property.RRULE).getValue());
		String startDate = new String(component.getProperty(Property.DTSTART).getValue());
		int endIndex = startDate.indexOf('T');
		String start = startDate.substring(0, endIndex);
		Date beginEventDate = new Date(start);
		String endDate = "20171031";
		Date endEventDate = new Date(endDate);
		DateList dates = eventRule.getRecur().getDates(beginEventDate, endEventDate, Value.DATE);
		return dates;
	}

	/**
	 * Returns the course schedule
	 * 
	 * @precondition None
	 * 
	 * @return The course schedule
	 */
	public ObservableList<LecturePeriod> getSchedule() {
		return this.list;
	}

	@Override
	protected void doAdd(int index, LecturePeriod element) {
		// TODO Auto-generated method stub

	}

	@Override
	protected LecturePeriod doRemove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected LecturePeriod doSet(int index, LecturePeriod element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LecturePeriod get(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compare(LecturePeriod lect1, LecturePeriod lect2) {
		if (lect1.dateProperty().getValue().isBefore(lect2.dateProperty().getValue())) {
			return -1;
		}
		if (lect1.dateProperty().getValue().isAfter(lect2.dateProperty().getValue())) {
			return 1;
		}
		return 0;
	}

}
