package edu.westga.syllabytes.model;

import java.time.LocalDate;
import java.time.ZoneId;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.RRule;

/**
 * Class that models a lecture period by taking information from a VEvent and
 * storing information for each lecture period
 * 
 * @author Tammy Lee
 * 
 * @version CS3211
 *
 */
public class LecturePeriod {

	private StringProperty title;
	private StringProperty description;
	private ObjectProperty<LocalDate> date;
	private RRule rule;

	/**
	 * Creates a lecture period with the specified VEvent
	 * 
	 * @precondition event != null
	 * 
	 * @postcondition a new LecturePeriod is created with specified VEvent
	 * 
	 * @param event
	 *            The event to create a LecturePeriod from
	 */
	public LecturePeriod(VEvent event) {
		if (event == null) {
			throw new IllegalArgumentException("Event must not be null!");
		}
		this.title = new SimpleStringProperty(event.getSummary().getValue());
		this.description= new SimpleStringProperty(event.getDescription().getValue());
		this.date = new SimpleObjectProperty<LocalDate>(getDate(event));


	}
	
	/**
	 * Returns the local date of the event
	 * @param event The event
	 * @return The date of the vevent
	 */
	private LocalDate getDate(VEvent event){
		return event.getStartDate().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	/**
	 * Gets the titleProperty
	 * 
	 * @precondition None
	 * @return The titleProperty
	 */
	public StringProperty titlePropety() {
		return this.title;
	}

	/**
	 * Gets the descriptionProperty
	 * 
	 * @precondition None
	 * @return The descriptionProperty
	 */
	public StringProperty descriptionProperty() {
		return this.description;
	}

	/**
	 * Gets the dateProperty
	 * 
	 * @precondition None
	 * @return The dateProperty
	 */
	public ObjectProperty<LocalDate> dateProperty() {
		return this.date;
	}
}
